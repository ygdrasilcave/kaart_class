/*
 *	IanniX Score File
 */


/*
 *	This method is called first.
 *	It is the good section for asking user for script global variables (parameters).
 *	
 * 	This section is never overwritten by IanniX when saving.
 */
function askUserForParameters() {
	//title("The title of the parameter box");
	//ask("Group name of the parameter (only for display purposes)", "Parameter label", "myGlobalVar", "theDefaultValue");
}


/*
 *	This method stores all the operations made through IanniX scripts.
 *	You can add some commands here to make your own scripts!
 *	Scripts are written in Javascript but even with a limited knowledge of Javascript, many types of useful scripts can be created.
 *	
 *	Beyond the standard javascript commands, the run() function is used to send commands to IanniX.
 *	Commands must be provided to run() as a single string.
 *	For example, run("zoom 100"); sets the display zoom to 100%.
 *	
 *	To combine numeric parameters with text commands to produce a string, use the concatenation operator.
 *	In the following example center_x and center_y are in numeric variables and must be concatenated to the command string.
 *	Example: run("setPos current " + center_x + " " + center_y + " 0");
 *	
 *	To learn IanniX commands, perform an manipulation in IanniX graphical user interface, and see the Helper window.
 *	You'll see the syntax of the command-equivalent action.
 *	
 *	And finally, remember that most of commands must target an object.
 *	Global syntax is always run("<command name> <target> <arguments>");
 *	Targets can be an ID (number) or a Group ID (string name of group) (please see "Info" tab in Inspector panel).
 *	Special targets are "current" (last used ID), "all" (all the objects) and "lastCurve" (last used curve).
 *	
 * 	This section is never overwritten by IanniX when saving.
 */
function makeWithScript() {
	//Clears the score
	run("clear");
	//Resets rotation
	run("rotate 0 0 0");
	//Resets score viewport center
	run("center 0 0");
	//Resets score zoom
	run("zoom 100");
}


/*
 *	When an incoming message is received, this method is called.
 *		- <protocol> tells information about the nature of message ("osc", "midi", "direct…)
 *		- <host> and <port> gives the origin of message, specially for IP protocols (for OpenSoundControl, UDP or TCP, it is the IP and port of the application that sends the message)
 *		- <destination> is the supposed destination of message (for OpenSoundControl it is the path, for MIDI it is Control Change or Note on/off…)
 *		- <values> are an array of arguments contained in the message
 *	
 * 	This section is never overwritten by IanniX when saving.
 */
function onIncomingMessage(protocol, host, port, destination, values) {
	//Logs a message in the console (open "Config" tab from Inspector panel and see "Message log")
	console("Received on '" + protocol + "' (" + host + ":" + port + ") to '" + destination + "', " + values.length + " values : ");
	
	//Browses all the arguments and displays them in log window
	for(var valueIndex = 0 ; valueIndex < values.length ; valueIndex++)
		console("- arg " + valueIndex + " = " + values[valueIndex]);
}


/*
 *	This method stores all the operations made through the graphical user interface.
 *	You are not supposed to modify this section, but it can be useful to remove some stuff that you added accidentaly.
 *	
 * 	Be very careful! This section is automaticaly overwritten when saving a score.
 */
function madeThroughGUI() {
//GUI: NEVER EVER REMOVE THIS LINE
	run("zoom 140");
	run("center 3.008 -0.0281661");
	run("rotate 0 0 0");
	run("speed 4.49");


	run("add curve 1");
	run("setpos current -3.42068 0.605689 0");
	var points1 = [
		{x: -1.60255, y: 0.397091, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 2.25491, y: 0.808366, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 4.45309, y: -0.808364, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 4.33964, y: -2.85055, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 6.35345, y: -3.51709, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 7.75745, y: -0.567273, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 8.99127, y: 1.95709, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 10.3811, y: 0.709091, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 10.8065, y: -1.19127, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 9.89891, y: -2.68036, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 6.708, y: 0.0992727, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 4.76509, y: 3.03491, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 1.51745, y: 3.34691, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: -0.127636, y: -0.496364, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 1.872, y: -2.79382, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 2.94982, y: -2.94982, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 5.53091, y: 0.723273, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 6.32509, y: 2.34, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 10.764, y: 2.36836, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 11.7, y: -0.538909, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 10.8491, y: -4.26873, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: 8.29636, y: -4.87855, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: -0.326181, y: -3.94255, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
		{x: -0.326181, y: -3.94255, z: 0, c1x: 0, c1y: 0, c1z: 0, c2x: 0, c2y: 0, c2z: 0},
	];
	for(var i = 0 ; i < points1.length ; i++)
		run("setpointat current " + i + " " + points1[i].x + " " + points1[i].y + " " + points1[i].z + " " + points1[i].c1x + " " + points1[i].c1y + " " + points1[i].c1z + " " + points1[i].c2x + " " + points1[i].c2y + " " + points1[i].c2z);
	run("add cursor 2");
	run("setcurve current lastCurve");
	run("setpos current -4.21107 1.08937 0");
	run("setmessage current 20, osc://ip_out:port_out/cursor cursor_xPos cursor_yPos ,");
	run("setpattern current 0 0 1");
	run("settime current 5511.34479000033");
	run("setwidth current 1.2");
	run("setdepth current 0.6");


	run("add trigger 17");
	run("setpos current 0.0193701 3.45028 0");
	run("setmessage current 1, osc://ip_out:port_out/color 1.0 0.5 0.0 ,");
	run("setcoloractive current _simple_curve_inactive");

	run("add trigger 3");
	run("setpos current -0.767045 1.9805 0");
	run("setmessage current 1, osc://ip_out:port_out/trigger 1 ,");

	run("add trigger 4");
	run("setpos current 3.20407 2.99237 0");
	run("setmessage current 1, osc://ip_out:port_out/trigger 2 ,");

	run("add trigger 5");
	run("setpos current 4.80552 -3.93801 0");
	run("setmessage current 1, osc://ip_out:port_out/trigger 3 ,");

	run("add trigger 6");
	run("setpos current 3.44774 -1.9876 0");
	run("setmessage current 1, osc://ip_out:port_out/trigger 1 ,");

	run("add trigger 7");
	run("setpos current -0.985388 -3.86281 0");
	run("setmessage current 1, osc://ip_out:port_out/trigger 2 ,");

	run("add trigger 8");
	run("setpos current -1.31266 -1.70049 0");
	run("setmessage current 1, osc://ip_out:port_out/trigger 3 ,");

	run("add trigger 9");
	run("setpos current -3.75663 1.76628 0");
	run("setmessage current 1, osc://ip_out:port_out/color 1.0 0.5 0.0 ,");
	run("setcoloractive current _simple_curve_inactive");

	run("add trigger 10");
	run("setpos current -0.00899363 1.77683 0");
	run("setmessage current 1, osc://ip_out:port_out/color 0.0 1.0 1.0 ,");
	run("setcoloractive current _simple_curve_active");

	run("add trigger 11");
	run("setpos current 1.38446 -0.297357 0");
	run("setmessage current 1, osc://ip_out:port_out/color 0.843 0.065 1.0 ,");
	run("setcoloractive current _simple_curve_active");

	run("add trigger 12");
	run("setpos current 7.167 2.47174 0");
	run("setmessage current 1, osc://ip_out:port_out/color 1.0 0.5 0.0 ,");
	run("setcoloractive current _simple_curve_inactive");

	run("add trigger 13");
	run("setpos current 7.05719 -0.141352 0");
	run("setmessage current 1, osc://ip_out:port_out/color 0.0 1.0 1.0 ,");
	run("setcoloractive current _simple_curve_active");

	run("add trigger 14");
	run("setpos current 1.35973 -4.04499 0");
	run("setmessage current 1, osc://ip_out:port_out/color 0.843 0.065 1.0 ,");
	run("setcoloractive current _simple_curve_active");

	run("add trigger 15");
	run("setpos current 5.82337 -4.12644 0");
	run("setmessage current 1, osc://ip_out:port_out/color 1.0 0.5 0.0 ,");
	run("setcoloractive current _simple_curve_inactive");

	run("add trigger 16");
	run("setpos current 7.24519 -3.69044 0");
	run("setmessage current 1, osc://ip_out:port_out/color 0.0 1.0 1.0 ,");
	run("setcoloractive current _simple_curve_active");



//GUI: NEVER EVER REMOVE THIS LINE
}


/*
 *	This method stores all the operations made by other softwares through one of the IanniX interfaces.
 *	You are not supposed to modify this section, but it can be useful to remove some stuff that you or a third party software added accidentaly.
 *	
 * 	Be very careful! This section is automaticaly overwritten when saving a score.
 */
function madeThroughInterfaces() {
//INTERFACES: NEVER EVER REMOVE THIS LINE

//INTERFACES: NEVER EVER REMOVE THIS LINE
}


/*
 *	This method is called last.
 *	It allows you to modify your hand-drawn score (made through graphical user interface).
 *	
 * 	This section is never overwritten by IanniX when saving.
 */
function alterateWithScript() {
	
}


/*
 *	//APP VERSION: NEVER EVER REMOVE THIS LINE
 *	Made with IanniX 0.9.17
 *	//APP VERSION: NEVER EVER REMOVE THIS LINE
 */



/*
    This file is part of IanniX, a graphical real-time open-source sequencer for digital art
    Copyright (C) 2010-2015 — IanniX Association

    Project Manager: Thierry Coduys (http://www.le-hub.org)
    Development:     Guillaume Jacquemin (http://www.buzzinglight.com)

    This file was written by Guillaume Jacquemin.

    IanniX is a free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

